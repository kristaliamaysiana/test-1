public class City {
    public static void main(String[] args) {
        infoCity("Kabupaten Aceh Barat", 0);
        infoCity("Kabupaten Aceh Besar", 0);
        infoCity("Kabupaten Batanghari", 1001);
        infoCity("Kabupaten Batanghari", 999);
        infoCity("Kabupaten Bungo", 0);
        infoCity("Kabupaten Lampung Timur", 0);
        infoCity("Kabupaten Lampung Utara", 0);
        infoCity("Kabupaten Bangli", 1001);
        infoCity("Kabupaten Buleleng", 1001);
        infoCity("Kabupaten Asmat", 0);
        infoCity("Kabupaten Biak Numfor", 0);
    }

    public static void infoCity(String city, int population) {
        switch(city) {
            case "Kabupaten Aceh Barat":
                System.out.println("Buah Pisang");
                break;
            case "Kabupaten Lampung Timur":
            case "Kabupaten Lampung Utara":
                System.out.println("Buah Apel");
                break;
            case "Kabupaten Aceh Besar":
                for (int i = 1; i <= 10; i++) {
                    System.out.println(i);
                }
                break;
            case "Kabupaten Bungo":
                for (int i = 1; i <= 10; i++) {
                    if (i == 5) {
                        break;
                    }
                    System.out.println(i);
                }
                break;
            case "Kabupaten Asmat":
            case "Kabupaten Biak Numfor":
                for (int i = 30; i <= 40; i++) {
                    System.out.println(i);
                }
                break;
            case "Kabupaten Batanghari":
            case "Kabupaten Bangli":
            case "Kabupaten Buleleng":
                if (population < 1000) {
                    System.out.println("Jumlah penduduk " + city + " < 1000.");
                } else if (population > 1000) {
                    System.out.println("Jumlah penduduk " + city + " > 1000.");
                } else {
                    System.out.println("Jumlah penduduk " + city + " = 1000.");
                }
                break;
        }
    }

}
