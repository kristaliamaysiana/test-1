package OOP;

public class Obat {
    public int id;
    public String namaObat;
    public int hargaObat;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNamaObat() {
        return namaObat;
    }

    public void setNamaObat(String namaObat) {
        this.namaObat = namaObat;
    }

    public int getHargaObat() {
        return hargaObat;
    }

    public void setHargaObat(int hargaObat) {
        this.hargaObat = hargaObat;
    }
}
