package OOP3;

public class Main {
    public static void main(String[] args) {
        Mobil mobil = new Mobil();
        System.out.println(mobil.getName());
        mobil.setBahanBakar("Pertamax");
        System.out.println(mobil.getBahanBakar());
        mobil.startEngine();

        Mobil wagoner = new Mobil("Karimun", "Hijau Metallic");
        System.out.printf("Ini mobil %s, warna %s\n", wagoner.getName(), wagoner.getColor());
        wagoner.startEngine();
        System.out.println(mobil.drift());
    }
}
