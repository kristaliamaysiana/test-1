package OOP3;

public class Kendaraan {
    private String name;
    private String color;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Kendaraan(String name, String color) {
        this.name = name;
        this.color = color;
    }

    public Kendaraan() {
    }

    public void startEngine() {
        System.out.println("Vrooom");
    }

    public String drift() {
        return "Ciiiiiiit";
    }
}
