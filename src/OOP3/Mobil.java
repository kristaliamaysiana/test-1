package OOP3;

public class Mobil extends Kendaraan {
    private String bahanBakar;

    protected String getBahanBakar() {
        return bahanBakar;
    }

    public Mobil(String name, String color) {
        super(name, color);
    }

    protected void setBahanBakar(String bahanBakar) {
        this.bahanBakar = bahanBakar;
    }

    protected Mobil() {
        super.setColor("Hitam");
        super.setName("Honda");
    }

    @Override
    public void startEngine() {
        System.out.println("Ngeeeeng");
    }
    public String drift() {
        return "Yeeeeeet";
    }
}
