package OOP2;

public class ClassBE {
    String nama;
    int umur;

    String m_judul() {
        return "Class BE Method final Anggota";
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getUmur() {
        return umur;
    }

    public void setUmur(int umur) {
        this.umur = umur;
    }

    String m_anggota(String nama, int umur) {
        nama = this.nama;
        umur = this.umur;
        System.out.println("method m_anggota pada class ClassBE");
        return "nama = "+getNama()+" umur = "+getUmur();
    }
}
